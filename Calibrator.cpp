//=======================================================================================
//--- CySD calibrator ------------------------------------------------------------------|
// Written by Daniel Santiago-Gonzalez                                                  |
// ver 1.0 (2016/6)                                                                     |
// To get the latest version type:                                                      |
// git clone https://dasago@bitbucket.org/dasago/cysd.git                               |
//                                                                                      |
// Compile with:                                                                        |
// g++ -Wall -I/usr/include/root -c Calibrator.cpp                                      |
// g++ Calibrator.o CySD.o `root-config --cflags --glibs` -lSpectrum -o CySD_calibrator |
//                                                                                      |
// Usage: ./CySD_calibrator -f /home/data/r200.root -c s1i.control                      |
//                                                                                      |
// Options specified on the command line                                                |
// -f <filename>   -- a single .root input file                                         |
// -c <filename>   -- text file with the control parameters                             |
//=======================================================================================

// C and C++ headers
#include <iostream>
#include <iomanip>
#include <unistd.h>   // For getopt()

// ROOT headers
#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraph.h>
#include <TH2F.h>
#include <TSpectrum.h>
#include <TStopwatch.h>
#include <TTree.h>

// Detector header
#include "CySD.hpp"

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////////
// Structure for storing arguments from specified with the command line
//////////////////////////////////////////////////////////////////////////////////////////
typedef struct {
  char datafile[1000];
  char ctrlfile[1000];
} gParameters;

// Default arguments values
gParameters CtrlParams = {"/home/dasago/Data/GSFMA339/ROOT_files/r258.root",
			  "s1i.control"};



//////////////////////////////////////////////////////////////////////////////////////////
// Auxiliary functions (declared after main)
//////////////////////////////////////////////////////////////////////////////////////////
// Function for reading arguments from the command line
int GetArgs(int argc,char **argv);
// Calibrating function (peak finding and fitting)
void Calibrate(CySD* Det, int MaxCh, int shift, TH1I** Hist, double* PeakSigma, double* MinPeakAmp, 
	       int* NPeaks, int** ActiveAlpha, double** AlphaEne, TCanvas* Can, string spec_name);




//////////////////////////////////////////////////////////////////////////////////////////
// CySD calibrator
//////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
  TCanvas* Can = 0;
  CySD* MyDet = 0;
  TFile* RF = 0;
  TTree* DetTree = 0;
  double** AlphaEne;
  int** ActiveAlpha;
  double* PeakSigma;
  double* MinPeakAmp;
  int* NPeaks;

  cout << "================================================================================" << endl;
  cout << "|--- Cylindrical Silicon Detector (CySD) Calibrator ---------------------------|" << endl; 
  cout << "| Written by Daniel Santiago-Gonzalez                                          |" << endl;
  cout << "| ver 1.0 (2016/6)                                                             |" << endl;
  cout << "| To get the latest version type:                                              |" << endl;
  cout << "| git clone https://dasago@bitbucket.org/dasago/cysd.git                       |" << endl;
  cout << "================================================================================" << endl;

  GetArgs(argc, argv);
 
  // Open the root file
  RF = new TFile(CtrlParams.datafile);
  if (!RF->IsOpen()) {
    // No need for error message. TFile will output one.
    return 0;
  }
  // Once the root file is open, assign the TTree 'dett' to the
  // DetTree pointer
  RF->GetObject("dett", DetTree);
  // Just a precaution
  if (!DetTree) { 
    delete RF; 
    cout << "Error: TTree not found." << endl;
    return 0; 
  }

  // Create the Cylindrical Silicon Detector (CySD) object
  MyDet = new CySD((string)(CtrlParams.ctrlfile));
 
  // Assign the branch elements to the CySD members
  DetTree->SetBranchAddress(Form("%s.ahits",MyDet->GetName()), &MyDet->ahits);
  DetTree->SetBranchAddress(Form("%s.ach",  MyDet->GetName()),  MyDet->ach);
  DetTree->SetBranchAddress(Form("%s.aval", MyDet->GetName()),  MyDet->aval);
  DetTree->SetBranchAddress(Form("%s.phits",MyDet->GetName()), &MyDet->phits);
  DetTree->SetBranchAddress(Form("%s.pch",  MyDet->GetName()),  MyDet->pch);
  DetTree->SetBranchAddress(Form("%s.pval", MyDet->GetName()),  MyDet->pval);
  

  long int TotEntries = DetTree->GetEntries();
  cout << "Processing " << TotEntries << " entries ..." << endl;

  // For progress monitor
  TStopwatch StpWatch;
  long EvtsProcessed = 0;
  long double Frac[6] = {0.01, 0.25, 0.5, 0.75, 0.9, 1.0};
  int FIndex = 0;

  // Loop over the TTree entries and save the data in the CySD
  // histogrmas. This is done in the SortHits method.
  for (int e=0; e<TotEntries; e++) {
    DetTree->GetEntry(e);
    MyDet->SortHits();

    // Simple progress monitor
    EvtsProcessed++;
    if (TotEntries>99) {
      if ((long double)(EvtsProcessed)>=Frac[FIndex]*TotEntries) {
	cout << "\t" << Frac[FIndex]*100 << "% processed (" << StpWatch.RealTime() << " s)" << endl;
	StpWatch.Start(kFALSE);
	FIndex++;
      }
    }
  }
  StpWatch.Print();

  // Look for the alpha peaks in each channel
  Can = new TCanvas("Can","",1400,800);
  Can->Divide(2);

  // For each channel, get the alpha source expected features
  // NumAlphaEne = MyDet->GetArrayNumAlphaEne();
  NPeaks = MyDet->GetArrayNumPeaks();  
  ActiveAlpha = MyDet->GetArrayActiveAlpha();
  AlphaEne = MyDet->GetArrayAlphaEne();
  
  // Get the peak-finding parameters for each channel
  PeakSigma = MyDet->GetArrayPeakSigma();
  MinPeakAmp = MyDet->GetArrayMinPeakAmp();


  // Calibrate azimuthal channels
  int shift = 0;
  int MaxCh = MyDet->NumAzimuthalChannels();
  Calibrate(MyDet, MaxCh, shift, MyDet->HAC, PeakSigma, MinPeakAmp, NPeaks, ActiveAlpha,
	    AlphaEne, Can, "spectra/ach");
    
  // Calibrate polar channels
  shift = MyDet->NumAzimuthalChannels();
  MaxCh = MyDet->NumPolarChannels();
  Calibrate(MyDet, MaxCh, shift, MyDet->HPC, PeakSigma, MinPeakAmp, NPeaks, ActiveAlpha,
	    AlphaEne, Can, "spectra/pch");
  
  // Write coefficients to text file
  MyDet->WriteCoefficients((string)(CtrlParams.ctrlfile));

  return 0;
}



//////////////////////////////////////////////////////////////////////////////////////////
// Function for reading arguments from the command line. Right now, this function assumes
// the user knows how to use the argumets.
// Refrecences:
// http://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
// http://pubs.opengroup.org/onlinepubs/009695399/functions/getopt.html
//////////////////////////////////////////////////////////////////////////////////////////
int GetArgs(int argc, char**argv) {
  int opt;
  
  while ((opt = (char)getopt(argc, argv, "f:c:")) != -1) {
    switch (opt) {
    case 'f': 
      strcpy(CtrlParams.datafile, optarg); 
      break;
    case 'c':
      strcpy(CtrlParams.ctrlfile, optarg);
      break;
    }
  }  
  
  return 1;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Calibrate
//////////////////////////////////////////////////////////////////////////////////////////
void Calibrate(CySD* Det, int MaxCh, int shift, TH1I** Hist, double* PeakSigma, double* MinPeakAmp, 
	       int* NPeaks, int** ActiveAlpha, double** AlphaEne, TCanvas* Can, string spec_name)
{
  TH2F* BckGnd = new TH2F("BckGnd","",100,0,4096,100,0,10);
  BckGnd->GetXaxis()->SetTitle("Energy [arb. units]");
  BckGnd->GetXaxis()->CenterTitle();
  BckGnd->GetYaxis()->SetTitle("Energy [MeV]");
  BckGnd->GetYaxis()->CenterTitle();
  BckGnd->SetStats(0);  

  string FileName = Det->GetName();
  FileName += ".log";
  ofstream LogFile(FileName.c_str(), std::ofstream::out | std::ofstream::app);
  
  for (int ch=0; ch<MaxCh; ch++) {
    LogFile << "*** Calibrating channel " << ch+shift << " ***" << endl;
    Can->cd(1);
    // NOTE: the TSpectrum::Search() method below only looks for peaks
    // in the current active region of the histogram, hence the use of
    // SetRangeUser.
    Hist[ch]->GetXaxis()->SetRangeUser(1000, 4000);
    Hist[ch]->Draw();

    TSpectrum* Peaks = new TSpectrum();
    int PeaksFound = Peaks->Search(Hist[ch], PeakSigma[ch+shift], " ", MinPeakAmp[ch+shift]);
    if(PeaksFound != NPeaks[ch+shift])
      LogFile << "WARNING: number of peaks found (" << PeaksFound 
	   << ") is not the same as the number of expected peaks (" << NPeaks[ch+shift] << ")." 
	   << endl;

    float* PeakX = Peaks->GetPositionX();
    float Temp=0;
    // Reorder the peaks' x-coordinate from lower to higher x-value.
    for(int i=0; i<PeaksFound; i++) {
      for(int j=i; j<PeaksFound; j++) {
	if (PeakX[j] < PeakX[i]) {
	  Temp = PeakX[i];
	  PeakX[i] = PeakX[j];
	  PeakX[j] = Temp;  
	}
      }
    }
    // Print peaks positions
    LogFile << "Peaks' positions:\t\tAlpha energies:" << endl;
    int TotLines = PeaksFound;
    if (PeaksFound<=NPeaks[ch+shift])
      TotLines = NPeaks[ch+shift];
    
    for(int i=0; i<TotLines; i++) { 
      LogFile << "\t" << setw(3) << i;
      if (i<PeaksFound)
	LogFile << setw(6) << PeakX[i];
      else
	LogFile << setw(6) << "---";
      LogFile << "\t" << setw(3) << i;
      if (i<NPeaks[ch+shift] && ActiveAlpha[ch+shift][i])
	LogFile << setw(8) << AlphaEne[ch+shift][i];
      else
	LogFile << setw(8) << "---";
      LogFile << "\n";
    }
    
    int NumActiveAlpha = 0;
    for (int na=0; na<NPeaks[ch+shift]; na++)
      if (ActiveAlpha[ch+shift][na])
	NumActiveAlpha++;

    float* GraphX = new float[NumActiveAlpha];
    float* GraphY = new float[NumActiveAlpha];
    int gp = 0;
    for (int na=0; na<NPeaks[ch+shift]; na++) {
      if (ActiveAlpha[ch+shift][na]) {
	GraphX[gp] = PeakX[na];
	GraphY[gp] = AlphaEne[ch+shift][na];
	gp++;
      }
    }
    
    TGraph* FitGraph = new TGraph(NumActiveAlpha, GraphX, GraphY);
    FitGraph->SetMarkerStyle(20);
    FitGraph->SetMarkerSize(1);
    
    // Variable: x = Qtot.
    // Parameters' indices:
    // 1 - CE1
    // 0 - CE0
    double CE1 = 1.0;
    double CE0 = 0.0;
    TF1* FitFunc = new TF1("FitFunc", "pol1", 500.0,4096.0);
    FitFunc->SetParNames("CE0","CE1");
    FitFunc->SetParameters(CE0, CE1);
    FitGraph->Fit(FitFunc,"RQM");
    
    CE0 = FitFunc->GetParameter(0);
    CE1 = FitFunc->GetParameter(1);
    
    //Extract the values of the parameters and store them for future use.
    LogFile << " Parameters found: " << endl;
    LogFile << "\tEoffset=" << CE0 << endl;
    LogFile << "\tEslope=" << CE1 << endl;
  
    Det->SetCoeffs(ch+shift, CE0, CE1);
      
    Can->cd(2);
    BckGnd->Draw();
    FitGraph->Draw("p same");
    Can->Update();
    Can->Print(Form("%s%d.png", spec_name.c_str(),ch));

    delete FitGraph;
    delete FitFunc;
    delete Peaks;
  }
  LogFile.close();
  delete BckGnd;
  return;
}
