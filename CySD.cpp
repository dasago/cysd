// Methods for CySD
// Class description in header file CySD.hpp
// Compile with: g++ -c `root-config --cflags --glibs` -o CySD.o CySD.cpp

#include "CySD.hpp"

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////////////////
// DSG_tree: simple class to store the Silicon detectors' data which then is passed to the TTree.
// The class is not saved in the root file.
//////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
// Constructor
////////////////////////////////////////////////////////////////////////////////////
CySD::CySD(string name, float zdet, float rmin, float rmax, int num_pch, int num_ach) 
{
  InitBasic(name, num_pch, num_ach);
  InitGeometry(zdet, rmin, rmax);
  Reset();
}


////////////////////////////////////////////////////////////////////////////////////
// Constructor
////////////////////////////////////////////////////////////////////////////////////
CySD::CySD(string ControlFile)
{
  if (ReadControlFile(ControlFile.c_str())) {
    Reset();
  }
  else {
    cout << "Error: Control file " << ControlFile << " not found!" << endl;
    // this = NULL; // compiling error
  }
  
}


////////////////////////////////////////////////////////////////////////////////////
// Azimuthal signal (wedges)
////////////////////////////////////////////////////////////////////////////////////
void CySD::AddAzimuthalSignal(int val, int ch)
{
  int h = ahits;
  if (h<MaxHits) {
    aval[h] = val;
    ach[h] = ch;
  }
  ahits++;
  hits++;
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Polar signal (rings)
////////////////////////////////////////////////////////////////////////////////////
void CySD::AddPolarSignal(int val, int ch)
{
  int h = phits;
  if (h<MaxHits) {
    pval[h] = val;
    pch[h] = ch;
  }
  phits++;
  hits++;
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Event type
// 1a,0p: possibly noise induced in azimuthal channel
// 0a,1p: possibly noise induced in polar channel
// 1a,1p: 
//
// 1a,2p: potential charge-sharing event (must check if polar channels
//        are contiguous and if total polar energy matches the
//        azimuthal energy)
////////////////////////////////////////////////////////////////////////////////////
int CySD::ClusterHits(double EneThresh, double Diff) 
{
  int MultP = 0;
  int MultA = 0;
  int* good_ah = new int[MaxHits];
  int* good_ph = new int[MaxHits];
  int* skip_ah =  new int[MaxHits];

  // Calculate the calibrated energy for azimuthal hits
  for (int ah=0; ah<ahits; ah++) {
    int ch = ach[ah];
    enea[ah] = Eslope[ch]*aval[ah] + Eoffset[ch];
    if (enea[ah]>EneThresh) {
      good_ah[MultA] = ah;
      MultA++;
    }
  }
  // Calculate the calibrated energy for polar hits
  for (int ph=0; ph<phits; ph++) {
    int ch = pch[ph] + num_ach;
    enep[ph] = Eslope[ch]*pval[ph] + Eoffset[ch];
    if (enep[ph]>EneThresh) {
      good_ph[MultP] = ph;
      MultP++;
    }
  }

  // Determine the event type
  evts = 0;
  for (int e=0; e<MaxEvts; e++)
    evttype[e] = 0;

  // Type: 1a,1p
  for (int ma=0; ma<MultA; ma++) {
    skip_ah[ma] = 0;
    int gah = good_ah[ma];
    for (int mp=0; mp<MultP; mp++) {
      int gph = good_ph[mp];
      if (fabs(enep[gph]-enea[gah])<Diff) {
	skip_ah[ma] = 1;
	evte[evts] = enep[gph];
	evttype[evts] = 1;
	evts++;
      } 
    }
  }
  // Type: 1a,2p
  for (int ma=0; ma<MultA; ma++) {
    if (!skip_ah[ma]) {
      int gah = good_ah[ma];
      for (int mp1=0; mp1<MultP; mp1++) {
	int gph1 = good_ph[mp1];
	for (int mp2=mp1+1; mp2<MultP-1; mp2++) {
	  int gph2 = good_ph[mp2];
	  // Potential charge-sharing event
	  // Check if the polar channels are contiguous
	  if (abs(pch[gph2]-pch[gph1])==1) {
	    // Then, check if total energy of the two polar hits matches the
	    // energy in the azimuthal hit
	    if (fabs((enep[gph2]+enep[gph1])-enea[gah])<5*Diff) {
	      evte[evts] = enea[gah];
	      evttype[evts] = 2;
	      evts++;
	    }
	  }	
	}
      }
    }
  }

  // else if (MultA==1 && MultP==2) {
  //   evttype = 2;
  //   int gah = good_ah[0];
  // }
  // Other types!
  // else if (MultA==2 && MultP=1) {
  //   // Potential charge-sharing event
  //   // First, check if the azimuthal channels are contiguous
  //   if (abs(ach[0]-ach[1])==1) {
  //     // Then, check if total energy of the two azimuthal hits matches
  //     // the energy in the polar hit
  //     if (fabs((enea[0]+enea[1])-enep[0])<Diff) {
  // 	evte[0] = enep[0];
  // 	evts = 1;
  //     } 
  //     else {

  //     }
  //   }
  delete[] skip_ah;
  delete[] good_ah;
  delete[] good_ph;
  return evts;
}


//////////////////////////////////////////////////////////////////////////////////
// Decode a string (List) where the delimiting character is a comma and save a
// specific number of list elements (NArrayElem) in a double array.
//////////////////////////////////////////////////////////////////////////////////
void CySD::DecodeList(string List, double* Array, int NArrayElem)
{
  size_t p1=0, p2=0;
  string ListElement = "";
  // It's easier to append a comma (delimiting character) to the end
  // of the string than doing a separate case for the last energy of
  // excitation.
  List.append(",");
  // Search for the list elements, separated by a comma (no spaces)
  // and assign them to the array elements.
  for (int i=0; i<NArrayElem; i++) {
    p2 = List.find(',', p1);
    if (p2!=string::npos) {
      ListElement = List.substr(p1, p2-p1);
      p1 = p2 + 1;
      Array[i] = atof(ListElement.c_str());
      // cout << Array[i] << "  ";
    }
  }
  //cout << endl;
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Returns a two-dimensional int array with 0 or 1 values for each channel and for 
// each alpha peak index. The matrix elements are determined by the active alpha key.
////////////////////////////////////////////////////////////////////////////////////
int** CySD::GetArrayActiveAlpha()
{
  int** array = new int*[MaxChans];
  // loop over the detector channels
  for (int ch=0; ch<MaxChans; ch++) {
    array[ch] = new int[NumPeaks[ch]];
    // loop over the alpha-peak index
    for (int ai=0; ai<NumPeaks[ch]; ai++) {
      array[ch][ai] = ActiveAlphaEne[ch]&(ai+1);
      //cout << ch << " " << ai << " " << array[ch][ai] << endl;
    }
  }
  return array;
}

////////////////////////////////////////////////////////////////////////////////////
// Returns a two-dimensional double array with the alpha energy values for each 
// channel and alpha-peak index.
////////////////////////////////////////////////////////////////////////////////////
double** CySD::GetArrayAlphaEne()
{
  double** array = new double*[MaxChans];
  // loop over the detector channels
  for (int ch=0; ch<MaxChans; ch++) {
    array[ch] = new double[NumPeaks[ch]];
    // loop over the alpha-peak index
    for (int ai=0; ai<NumPeaks[ch]; ai++) {
      array[ch][ai] = AlphaEne[ch][ai];
      //cout << ch << " " << ai << " " << array[ch][ai] << endl;
    }
  }
  return array;
}


////////////////////////////////////////////////////////////////////////////////////
// Returns an array with the minimum peak amplitude for each channel.
////////////////////////////////////////////////////////////////////////////////////
double* CySD::GetArrayMinPeakAmp() 
{
  double* array = new double[MaxChans];
  for (int ch=0; ch<MaxChans; ch++)
    array[ch] = MinPeakAmp[ch];
  return array;
}


////////////////////////////////////////////////////////////////////////////////////
// Returns an array with the number of alpha peaks expected for each channel.
////////////////////////////////////////////////////////////////////////////////////
int* CySD::GetArrayNumPeaks()
{
  int* array = new int[MaxChans];
  for (int ch=0; ch<MaxChans; ch++)
    array[ch] = NumPeaks[ch];
  return array;
}


////////////////////////////////////////////////////////////////////////////////////
// Returns an array with the peak sigma (std. dev.) for each channel.
////////////////////////////////////////////////////////////////////////////////////
double* CySD::GetArrayPeakSigma()
{
  double* array = new double[MaxChans];
  for (int ch=0; ch<MaxChans; ch++)
    array[ch] = PeakSigma[ch];
  return array;
}


////////////////////////////////////////////////////////////////////////////////////
// Return the calibrated energy for a given azimuthal hit. 
////////////////////////////////////////////////////////////////////////////////////
float CySD::GetEneA(int ah)
{
  float ene = 0;
  if (ah>=0 && ah<ahits) {
    int ch = ach[ah];
    if ((int)(Eslope[ch])==1 && (int)(Eoffset[ch])==0)
      ene = 0; // channels not used
    else
      ene = Eslope[ch]*aval[ah] + Eoffset[ch];
  }
  return ene;
}


////////////////////////////////////////////////////////////////////////////////////
// Return the calibrated energy for a given polar hit. 
////////////////////////////////////////////////////////////////////////////////////
float CySD::GetEneP(int ph)
{
  float ene = 0;
  if (ph>=0 && ph<phits) {
    int ch = pch[ph] + num_ach;
    if ((int)(Eslope[ch])==1 && (int)(Eoffset[ch])==0)
      ene = 0; // channels not used
    else
      ene = Eslope[ch]*pval[ph] + Eoffset[ch];
  }
  return ene;
}


////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////
float CySD::GetEventEne(int evt)
{
  float ene = 0;
  if (evt>=0 && evt<evts)
    ene = evte[evt];
  return ene;
}


////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////
int CySD::GetEventType(int evt)
{
  int type = 0;
  if (evt>=0 && evt<evts)
    type = evttype[evt];
  return type;
}


////////////////////////////////////////////////////////////////////////////////////
// WORK IN PROGRESS ...
////////////////////////////////////////////////////////////////////////////////////
double CySD::GetPhi(int ach)
{
  double phi = 0;
  double rdm = PRNG->Uniform();  // Random number between 0 and 1.
 
  
  return phi;
}


////////////////////////////////////////////////////////////////////////////////////
// Returns the polar angle (theta, in radians) for a given polar channel. It is 
// assumed that the hit can take place anywhere inside that polar (ring) channel
// (see rdm).
////////////////////////////////////////////////////////////////////////////////////
double CySD::GetTheta(int pch)
{
  double theta = 0;
  double rdm = PRNG->Uniform();  // Random number between 0 and 1.
  theta = atan(((rmax-rmin)*(pch+rdm)/num_pch + rmin)/zdet);
  if (zdet<0)
    theta += pi;
  return theta;
}


////////////////////////////////////////////////////////////////////////////////////
// Initialize basic mambers (related to polar and azimuthal channels)
////////////////////////////////////////////////////////////////////////////////////
void CySD::InitBasic(string name, int num_pch, int num_ach)
{
  // Set/create class members
  this->name = name;
  MaxHits = num_pch*num_ach;
  hits = MaxHits;
  this->num_ach = num_ach;
  this->num_pch = num_pch;
  ach = new int[MaxHits];
  pch = new int[MaxHits];
  aval = new int[MaxHits];
  pval = new int[MaxHits];
  enep = new float[MaxHits];
  enea = new float[MaxHits];
  
  // For now the maximum number of hits is the same as the maximum
  // number of events.
  MaxEvts = MaxHits;
  evttype = new int[MaxEvts];
  evte = new float[MaxEvts];
  evtx = new float[MaxEvts];
  evty = new float[MaxEvts];
  evtz = new float[MaxEvts];
  
  // Related to detector calibration
  MaxChans = num_pch + num_ach;
  PeakSigma = new double[MaxChans];
  MinPeakAmp = new double[MaxChans];
  NumPeaks = new int[MaxChans];
  ActiveAlphaEne = new int[MaxChans];
  AlphaEne = new double*[MaxChans];
  Eslope = new double[MaxChans];
  Eoffset = new double[MaxChans];
  // Maybe initialize here calib. coeffs.

  // Pseudo-random number generator
  PRNG = new TRandom3();
  PRNG->SetSeed();
  
  // 1-dimensional histograms for azimuthal and polar channels. For
  // calibration purposes, they must be filled only with a1p1 type
  // entries.
  HAC = new TH1I*[num_ach];
  for (int ch=0; ch<num_ach; ch++) {
    HAC[ch] = new TH1I(Form("%s_hac%d",name.c_str(),ch),
		       Form("Azimuthal ch %d (%s)",ch,name.c_str()),
		       1024,0,4096);
    HAC[ch]->GetXaxis()->SetTitle("Energy [arb. units]");
    HAC[ch]->GetXaxis()->CenterTitle();
  }
  HPC = new TH1I*[num_pch];
  for (int ch=0; ch<num_pch; ch++) {
    HPC[ch] = new TH1I(Form("%s_hpc%d",name.c_str(),ch),
		       Form("Polar ch %d (%s)",ch,name.c_str()),
		       1024,0,4096);
    HPC[ch]->GetXaxis()->SetTitle("Energy [arb. units]");
    HPC[ch]->GetXaxis()->CenterTitle();
  }
  
  MaxQSortIterations = int(MaxHits*log2(MaxHits));
  cout << "CySD created: name=" << name << " ach=" << num_ach << " pch=" << num_pch 
       << " max_hits=" << MaxHits << endl;
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Initialize geometry mambers
////////////////////////////////////////////////////////////////////////////////////
void CySD::InitGeometry(float zdet, float rmin, float rmax)
{
  this->zdet = zdet;
  this->rmin = rmin;
  this->rmax = rmax;
  return;
}



////////////////////////////////////////////////////////////////////////////////////
// Print the a list with the polar and azimuthal hit information
////////////////////////////////////////////////////////////////////////////////////
void CySD::PrintHitList()
{
  cout << phits << " polar hits:" << endl;
  for (int hit=0; hit<phits; hit++) {
    cout.width(10);
    cout << pch[hit];
    cout.width(10);
    cout << pval[hit] << endl;
  }
  cout << ahits << " azimuthal hits:" << endl;
  for (int hit=0; hit<ahits; hit++) {
    cout.width(10);
    cout << ach[hit];
    cout.width(10);
    cout << aval[hit] << endl;
  }
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Quicksort algorithm modified to get the largest azimuthal value first.  Source:
// http://www.algolist.net/Algorithms/Sorting/Quicksort
////////////////////////////////////////////////////////////////////////////////////
void CySD::QuickSortA(int left, int right) 
{
  int i = left, j = right;
  int tmp;
  int pivot = aval[(int)((left + right)/2)];  

  QSortCounter++;
  if (QSortCounter>MaxQSortIterations) {
    cout << "Warning: QuickSort reached maximum number of iterations (" 
	 << MaxQSortIterations << ")." << endl;
    return;
  }

  // Partition 
  while (i <= j) {
    while (aval[i] > pivot)
      i++;
    while (aval[j] < pivot)
      j--;
    if (i <= j) {
      // Change aval
      tmp = aval[i];
      aval[i] = aval[j];
      aval[j] = tmp;
      // Change ach
      tmp = ach[i];
      ach[i] = ach[j];
      ach[j] = tmp;
      i++;
      j--;
    }
  };
  
  // Recursion
  if (left < j)
    QuickSortA(left, j);
  if (i < right)
    QuickSortA(i, right);

  return;
}


/////////////////////////////////////////////////////////////////////////////////////
// Quicksort algorithm modified to get the largest polar raw value first.  Source:
// http://www.algolist.net/Algorithms/Sorting/Quicksort
////////////////////////////////////////////////////////////////////////////////////
void CySD::QuickSortP(int left, int right) 
{
  int i = left, j = right;
  int tmp;
  int pivot = pval[(int)((left + right)/2)];  

  QSortCounter++;
  if (QSortCounter>MaxQSortIterations) {
    cout << "Warning: QuickSort reached maximum number of iterations (" 
	 << MaxQSortIterations << ")." << endl;
    return;
  }

  // Partition 
  while (i <= j) {
    while (pval[i] > pivot)
      i++;
    while (pval[j] < pivot)
      j--;
    if (i <= j) {
      // Change pval
      tmp = pval[i];
      pval[i] = pval[j];
      pval[j] = tmp;
      // Change pch
      tmp = pch[i];
      pch[i] = pch[j];
      pch[j] = tmp;
      i++;
      j--;
    }
  };
  
  // Recursion
  if (left < j)
    QuickSortP(left, j);
  if (i < right)
    QuickSortP(i, right);

  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Read detector parameters from a control file
////////////////////////////////////////////////////////////////////////////////////
int CySD::ReadControlFile(string FileName) 
{
  int Status = 0;
  string line = "";
  ifstream ControlFile(FileName.c_str());
  if (ControlFile.is_open()) {
    do {
      getline(ControlFile, line);
      if (line.find("General controls")<string::npos) {
	// The following 6 quantities are expected
	// 1. Detector branch name: s1i
	getline(ControlFile, line);
	// In the line below, it is assumed that there is only one
	// empty space between the ':' and the detector branch name
	// (which must not contain empty spaces). A better approach
	// would be to try to remove the empty spaces from 'name', but
	// I decided to do it the pragmatic way, hence the '+2'.
	string name = line.substr(line.find(':')+2).c_str();     
	// 2. Azimuthal channels: 16
	getline(ControlFile, line);
	int num_ach = atoi(line.substr(line.find(':')+1).c_str());	
	// 3. Polar channels: 16
	getline(ControlFile, line);
	int num_pch = atoi(line.substr(line.find(':')+1).c_str());		
	// 4. Minimum active area radius (cm): 2.4
	getline(ControlFile, line);
	float zdet = atof(line.substr(line.find(':')+1).c_str());       
	// 5. Maximum active area radius (cm): 4.8
	getline(ControlFile, line);
	float rmin = atof(line.substr(line.find(':')+1).c_str());
	// 6. Position along the beam axis (cm): -5.5
	getline(ControlFile, line);
	float rmax = atof(line.substr(line.find(':')+1).c_str());       
	Status = 1;

	cout << "Detector branch name: " << name << endl;
	cout << "Azimuthal channels: " << num_ach << endl;
	cout << "Polar channels: " << num_pch << endl;

	InitBasic(name, num_pch, num_ach);
	InitGeometry(zdet, rmin, rmax);
      }
      else if (line.find("Calibration controls")<string::npos) {
	int chan, NPeaks, ActiveAlphas;
	double Sigma, MinAmp;
	string AlphaEneStr;
	// Azimuthal channels
	// First line is the column description for the azimuthal channels
	getline(ControlFile, line);
	for (int ach=0; ach<num_ach; ach++) {
	  ControlFile >> dec >> chan >> Sigma >> MinAmp >> NPeaks >> hex >> ActiveAlphas;
	  ControlFile >> dec >> AlphaEneStr;
	  // the last column is for comments
	  getline(ControlFile, line);
	  if (chan>=0 && chan<num_ach) {
	    PeakSigma[chan] = Sigma;
	    MinPeakAmp[chan] = MinAmp;
	    NumPeaks[chan] = NPeaks;
	    ActiveAlphaEne[chan] = ActiveAlphas;
	    AlphaEne[chan] = new double[NPeaks];
	    DecodeList(AlphaEneStr, AlphaEne[chan], NPeaks);
	    //  cout << dec << chan << " " << PeakSigma[chan] << " " << hex << ActiveAlphaEne[chan] 
	    //	 << dec << " " << line << endl;
	  }
	}
	// Polar channels
	// First line is the column description
	getline(ControlFile, line);
	getline(ControlFile, line);

	for (int pch=0; pch<num_pch; pch++) {
	  ControlFile >> dec >> chan >> Sigma >> MinAmp >> NPeaks >> hex >> ActiveAlphas;
	  ControlFile >> dec >> AlphaEneStr;
	  // the last column is for comments
	  getline(ControlFile, line);
	  if (chan>=0 && chan<num_pch) {
	    chan = chan + num_ach; // shift the channels
	    PeakSigma[chan] = Sigma;
	    MinPeakAmp[chan] = MinAmp;
	    NumPeaks[chan] = NPeaks;
	    ActiveAlphaEne[chan] = ActiveAlphas;
	    AlphaEne[chan] = new double[NPeaks];
	    DecodeList(AlphaEneStr, AlphaEne[chan], NPeaks);
	    // cout << dec << chan << " " << PeakSigma[chan] << " " << hex << ActiveAlphaEne[chan] 
	    // 	 << dec << " " << line << endl;
	  }
	}
	
      }
      else if (line.find("Calibration coefficients")<string::npos) {
	int chan;
	double Coeff1, Coeff0;
	// Azimuthal channels
	// First line is the column description for the azimuthal channels
	getline(ControlFile, line);
	for (int ach=0; ach<num_ach; ach++) {
	  ControlFile >> dec >> chan >> Coeff1 >> Coeff0;
	  // the last column is for comments
	  getline(ControlFile, line);
	  if (chan>=0 && chan<num_ach) {
	    Eslope[chan] = Coeff1;
	    Eoffset[chan] = Coeff0;
	    cout << dec << chan << " " << Eslope[chan] << " " << Eoffset[chan] << endl;
	  }
	}
	// Polar channels
	// First line is the column description
	getline(ControlFile, line);
	getline(ControlFile, line);
	for (int pch=0; pch<num_pch; pch++) {
	  ControlFile >> dec >> chan >> Coeff1 >> Coeff0;
	  // the last column is for comments
	  getline(ControlFile, line);
	  if (chan>=0 && chan<num_pch) {
	    chan = chan + num_ach; // shift the channels
	    Eslope[chan] = Coeff1;
	    Eoffset[chan] = Coeff0;
	    cout << dec << chan << " " << Eslope[chan] << " " << Eoffset[chan] << endl;
	  }
	}	
      }
      
    } while (!ControlFile.eof());
    ControlFile.close();
  }
  return Status;
}


////////////////////////////////////////////////////////////////////////////////////
// Reset members to default values
////////////////////////////////////////////////////////////////////////////////////
void CySD::Reset() 
{
  int Limit = hits;
  if (hits>MaxHits)
    Limit = MaxHits;
  for (int i=0; i<Limit; i++) {
    ach[i] = -1;
    pch[i] = -1;
    aval[i] = 0;
    pval[i] = 0;
    // ene[i] = 0;
    // x[i] = 0;     
    // y[i] = 0;
    // z[i] = -1000;
  }
  hits = 0;
  ahits = 0;
  phits = 0;
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Set the energy calibration coefficients for a particular channel.
////////////////////////////////////////////////////////////////////////////////////
void CySD::SetCoeffs(int ch, double offset, double slope)
{
  if (ch>=0 && ch<MaxChans) {
    Eoffset[ch] = offset;
    Eslope[ch] = slope;
  }
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Sort the hits data in descending order
////////////////////////////////////////////////////////////////////////////////////
void CySD::SortHits() {
  QSortCounter = 0;
  QuickSortP(0, phits-1); 
  QSortCounter = 0;
  QuickSortA(0, ahits-1);

  if (ahits==1)
    HAC[ach[0]]->Fill(aval[0]);
  if (phits==1)
    HPC[pch[0]]->Fill(pval[0]);
  return;
}


////////////////////////////////////////////////////////////////////////////////////
// Sort the hits data in descending order
////////////////////////////////////////////////////////////////////////////////////
void CySD::WriteCoefficients(string FileName)
{
  string line = "";
  int line_ctr = 1;
  ifstream ControlFile(FileName.c_str());
  if (ControlFile.is_open()) {
    // Read the control file into memory   
    // Get length of file
    ControlFile.seekg(0, ControlFile.end);
    int length = ControlFile.tellg();
    ControlFile.seekg(0, ControlFile.beg);
    // Create a buffer to store the control file text
    char* buffer = new char[length];
    // Read data as a block
    ControlFile.read(buffer, length);
    // Close the control file
    ControlFile.close();
    
    // Replace the calibration coefficients and rewrite the control
    // file (don't forget to skip (get) a line in the stringstream
    // when a new line is written by the ofstream).
    ofstream NewControlFile(FileName.c_str());    
    stringstream CFStream(buffer);
    while (!CFStream.eof()) {
      getline(CFStream, line);
      if (line.find("Calibration coefficients")<string::npos) {
	NewControlFile << line << endl;
	int chan;
	double Coeff1, Coeff0;
	// Write azimuthal channels
	// First line is the column description for the azimuthal channels
	NewControlFile.width(4);
	NewControlFile << left << "ach";
	NewControlFile.width(15);
	NewControlFile << left << "Eslope";
	NewControlFile.width(10);
	NewControlFile << left << "Eoffset" << endl;
	getline(CFStream, line);
	for (int ach=0; ach<num_ach; ach++) {
	  NewControlFile.width(4);
	  NewControlFile << left << ach;
	  NewControlFile.width(15);
	  NewControlFile << left << Eslope[ach];
	  NewControlFile.width(10);
	  NewControlFile << left << Eoffset[ach] << endl;
	  getline(CFStream, line);
	}
	NewControlFile << "\n";
	getline(CFStream, line);	
	// Write polar channels
	// First line is the column description
	NewControlFile.width(4);
	NewControlFile << left << "pch";
	NewControlFile.width(15);
	NewControlFile << left << "Eslope";
	NewControlFile.width(10);
	NewControlFile << left << "Eoffset" << endl;
	getline(CFStream, line);
	int shift = num_ach;
	for (int pch=0; pch<num_pch; pch++) {
	  NewControlFile.width(4);
	  NewControlFile << left << pch;
	  NewControlFile.width(15);
	  NewControlFile << left << Eslope[pch+shift];
	  NewControlFile.width(10);
	  NewControlFile << left << Eoffset[pch+shift] << endl;
	  getline(CFStream, line);
	}
	NewControlFile << "\n";
	getline(CFStream, line);
      }
      else {
	NewControlFile << line << endl;
      }
    }
    NewControlFile.close();
  }
  return;
}
