//////////////////////////////////////////////////////////////////////////////////////////////////
// CySD class
// Description: a class to store the data of cylindrical silicon detectors with rings and wedges.
// Compile with: g++ -c `root-config --cflags --glibs` -o CySD.o CySD.cpp
//
// By Daniel Santiago-Gonzalez
// 2016/3
//////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef CySD_hpp_INCLUDED   
#define CySD_hpp_INCLUDED   

//C and C++ headers
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>

// ROOT headers
#include <TH1.h>
#include <TRandom3.h>

class CySD {
public:
  // Constructor
  CySD(std::string name, float zdet, float rmin, float rmax, int num_pch, int num_ach);
  CySD(std::string ControlFile);
  void AddAzimuthalSignal(int val, int ch);
  void AddPolarSignal(int val, int ch);
  int ClusterHits(double EneThresh, double Diff);
  int** GetArrayActiveAlpha();
  double** GetArrayAlphaEne();
  double* GetArrayMinPeakAmp();
  int* GetArrayNumPeaks();
  double* GetArrayPeakSigma();
  float GetEneA(int ahit);
  float GetEneP(int phit);
  float GetEventEne(int evt);
  int GetEventType(int evt);
  const char* GetName() {return name.c_str();};
  int NumAzimuthalChannels() {return num_ach;};
  int NumPolarChannels() {return num_pch;};
  void PrintHitList();
  void Reset();
  void SetCoeffs(int ch, double offset, double slope);
  void SortHits();
  void WriteCoefficients(std::string FileName);
  
  // Public members used by TTree
  int hits;
  int ahits;
  int phits;
  int* ach;    // azimuthal channel (wedge)
  int* pch;    // polar channel (ring)
  int* aval;   // raw energy from azimuthal channel
  int* pval;   // raw energy from polar channel
  // float* ene;  // energy
  // float* x;  
  // float* y;
  // float* z;

  // Public histograms
  TH1I** HAC;
  TH1I** HPC;

 private:
  void DecodeList(std::string List, double* Array, int NArrayElem);
  double GetPhi(int ach);
  double GetTheta(int pch);
  void InitBasic(std::string name, int num_pch, int num_ach);
  void InitGeometry(float zdet, float rmin, float rmax);
  // Quicksort algorithm modified to get the largest values first.  Source:
  // http://www.algolist.net/Algorithms/Sorting/Quicksort
  void QuickSortA(int left, int right);
  void QuickSortP(int left, int right);
  int ReadControlFile(std::string ControlFile);
  

  // Private members
  std::string name;
  int MaxHits;
  int MaxChans;
  int MaxQSortIterations;
  int QSortCounter;
  float rmin;
  float rmax;
  float zdet;
  int num_ach;
  int num_pch;
  int* prelim_aval;  // is it used??
  int* prelim_pval;  // is it used??
  float* enea;
  float* enep;

  // Physical event
  int MaxEvts;
  int* evttype;
  int evts;
  float* evte;
  float* evtx;  
  float* evty;
  float* evtz;
  
  // Members related to detector calibration
  int* NumPeaks;
  double* PeakSigma;
  double* MinPeakAmp;
  double** AlphaEne;
  int* ActiveAlphaEne;
  double* Eslope;
  double* Eoffset;

  // Pseudo-random number generator
  TRandom3* PRNG;

  static const double pi = 3.14159265358979323846;


};

#endif
