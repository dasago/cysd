// Short program to test the functionality of the CySD class
// Compile with (2 steps):
// g++ -Wall -I/usr/include/root -c test_class.cpp
// g++ test_class.o CySD.o `root-config --cflags --glibs` -o test_class
// Reference: https://root.cern.ch/phpBB3/viewtopic.php?t=12889

#include <iostream>

#include <TCanvas.h>

#include "CySD.hpp"

TCanvas* Can;
CySD* MyDet;
CySD* MyOtherDet;

using namespace std;

int main()
{

  // Testing CySD object using the 1st constructor
  MyDet = new CySD("test", 0.0, 1.0, 10.0, 16, 16);
  cout << "> Address: " << MyDet << endl; 
  cout << "> Adding signals ...\n> *** Event 1 *************" << endl;
  MyDet->AddPolarSignal(1000,1);
  MyDet->AddPolarSignal(2000,2);
  MyDet->AddPolarSignal(500,3);
  MyDet->AddPolarSignal(200,4);
  MyDet->AddAzimuthalSignal(500,5);
  MyDet->AddAzimuthalSignal(1500,6);
  MyDet->AddAzimuthalSignal(600,7);
  cout << "> Sorting hits ..." << endl;
  MyDet->SortHits();
  MyDet->PrintHitList();
  cout << "> Reset" << endl;
  MyDet->Reset();
  cout << "> *** Event 2 *************" << endl;
  MyDet->AddPolarSignal(2100,4);
  MyDet->AddAzimuthalSignal(2200,5);
  MyDet->SortHits();
  MyDet->PrintHitList();

  Can = new TCanvas("Can","test",1000,800);
  Can->Divide(2);
  Can->cd(1);
  MyDet->HPC[4]->Draw();
  Can->cd(2);
  MyDet->HAC[5]->Draw();
  Can->Update();
  Can->Print("output.png");


  // Testing 2nd constrctor, where only the control filename is specified
  MyOtherDet = new CySD("s1i.control");
  cout << "> Address: " << MyOtherDet << endl; 
  cout << "> Adding signals ...\n> *** Event 1 *************" << endl;
  MyOtherDet->AddPolarSignal(1000,1);
  MyOtherDet->AddPolarSignal(2000,2);
  MyOtherDet->AddPolarSignal(500,3);
  MyOtherDet->AddPolarSignal(200,4);
  MyOtherDet->AddAzimuthalSignal(500,5);
  MyOtherDet->AddAzimuthalSignal(1500,6);
  MyOtherDet->AddAzimuthalSignal(600,7);
  cout << "> Sorting hits ..." << endl;
  MyOtherDet->SortHits();
  MyOtherDet->PrintHitList();
  cout << "> Reset" << endl;
  MyOtherDet->Reset();
  cout << "> *** Event 2 *************" << endl;
  MyOtherDet->AddPolarSignal(2100,4);
  MyOtherDet->AddAzimuthalSignal(2200,5);
  MyOtherDet->SortHits();
  MyOtherDet->PrintHitList();

  Can = new TCanvas("Can2","test2",1000,800);
  Can->Divide(2);
  Can->cd(1);
  MyOtherDet->HPC[4]->Draw();
  Can->cd(2);
  MyOtherDet->HAC[5]->Draw();
  Can->Update();
  Can->Print("output2.png");
  return 0;
}
